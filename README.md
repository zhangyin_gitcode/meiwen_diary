<p align="center">
<img alt="" src="https://raw.gitcode.com/zhangyin_gitcode/naivetoolkit/attachment/uploads/3243f369-7e18-49ba-942e-03c86e52f37f/naivetoolkit.png" style="display: inline-block;" />
</p>

<div align="center">
<h1>meiwen_diary</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.53.13-brightgreen" style="display: inline-block;" />
</p>

## 介绍

仓颉云原生开发示例项目。

### 项目结构

#### core

CRUD微服务，涵盖如下特性：

* RESTful API
* 简易的ORM

#### ai

AI微服务，涵盖如下特性：

* 大模型调用

## 使用说明

### SWR访问授权

参见 https://gitcode.com/zhangyin_gitcode/e-delivery

### 编译构建

```shell
docker compose up
```

## 开源协议

Apache License, Version 2.0

## 参与贡献

本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 实现并维护。技术支持和意见反馈请提Issue。

欢迎给我们提交PR，欢迎参与任何形式的贡献。

本项目committer：[@zhangyin_gitcode](https://gitcode.com/zhangyin_gitcode) (HUAWEI Developer Advocate)

![devadvocate.png](https://raw.gitcode.com/SIGCANGJIE/homepage/attachment/uploads/9b648c07-efc2-4eb3-b02f-eab18c77beea/devadvocate.png 'devadvocate.png')