/*!40101 SET NAMES utf8 */;

CREATE DATABASE meiwenDb;

USE meiwenDb;

CREATE TABLE diary (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content TEXT NOT NULL,
    entryTime TIMESTAMP NOT NULL,
    userIdentityGuid VARCHAR(255) NOT NULL,
    isDeleted TINYINT(1) NOT NULL DEFAULT 0,
    INDEX idx_entryTime (entryTime),
    INDEX idx_userIdentityGuid (userIdentityGuid),
    INDEX idx_isDeleted (isDeleted)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我决定开始一个新的开源项目。我打算开发一个简单的任务管理应用，帮助用户组织他们的日常任务。我选择了使用Python和Flask框架来开发后端，前端则使用React。我在GitHub上创建了一个新的仓库，并写了一些初步的文档说明项目的目标和功能。', '2024-09-02 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天主要是搭建项目的基本结构。我创建了Flask应用的基本框架，并设置了React的开发环境。我还写了一些基本的API接口，比如获取任务列表和添加新任务。虽然进展不算很大，但感觉一切都在按计划进行。', '2024-09-04 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我花了很多时间在调试上。前端和后端的接口通信出现了一些问题，导致数据无法正确传递。经过一番努力，我终于找到了问题所在，是因为CORS设置不正确。修复后，前端终于能够正确地从后端获取数据了。', '2024-09-06 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('周末休息了一下，今天继续开发任务管理应用。我添加了任务的编辑和删除功能，还为每个任务增加了优先级和截止日期的字段。为了更好地展示任务列表，我使用了Material-UI库来美化前端界面。', '2024-09-09 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我决定为应用添加用户认证功能。我研究了一些关于JWT（JSON Web Token）的资料，并在后端实现了用户注册和登录的API接口。前端也增加了相应的登录和注册页面。虽然功能还比较基础，但至少用户可以开始使用应用了。', '2024-09-11 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我主要在写测试代码。我认为测试是开发过程中非常重要的一部分，所以我为后端的API接口写了一些单元测试，确保它们能够正确工作。前端的测试还没有开始，但这是下周的计划之一。', '2024-09-13 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始为前端编写测试代码。我选择了Jest和React Testing Library来进行单元测试。虽然一开始有点不习惯，但经过一些练习后，我逐渐掌握了如何编写有效的测试用例。测试覆盖率逐渐提高，让我对代码的质量更有信心。', '2024-09-16 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我决定添加一些新的功能，比如任务的标签和搜索功能。用户可以为每个任务添加多个标签，并通过搜索框快速找到相关任务。这些功能的添加让应用变得更加实用和友好。', '2024-09-18 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我花了一些时间优化代码。去掉了一些冗余的代码，改进了一些性能瓶颈。特别是前端的渲染性能，通过使用React的memoization和虚拟滚动技术，任务列表的显示变得更加流畅。', '2024-09-20 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始考虑如何推广我的开源项目。我在GitHub上创建了一个详细的README文件，介绍项目的功能、安装步骤和使用指南。我还在README中添加了一些应用的截图，展示它的界面和使用方法。', '2024-09-23 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我在几个开发者社区和论坛上发布了我的项目，邀请其他开发者来试用和贡献。我还在Twitter和LinkedIn上分享了项目的链接，得到了不少正面的反馈和建议。', '2024-09-25 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我收到了一些来自社区的Pull Request，有人帮我修复了一些Bug，还有人添加了一些新的功能。我花了一些时间审查这些PR，并与贡献者进行了交流。这种合作的感觉真是太棒了。', '2024-09-27 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我决定为项目添加国际化支持。考虑到用户可能来自不同的国家和地区，我使用了i18next库来实现多语言支持。虽然目前只支持英文和中文，但以后可以根据需求添加更多语言。', '2024-09-30 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始编写项目的文档，包括API文档和开发者指南。我希望通过详细的文档，能够吸引更多的开发者参与到项目中来。文档写作虽然有点枯燥，但我知道这是非常重要的一部分。', '2024-10-02 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为项目添加了持续集成和持续部署（CI/CD）管道。我使用GitHub Actions来实现自动化测试和部署，每次代码提交后都会自动运行测试，并将最新版本部署到Heroku上。这样可以确保每个版本都是稳定和可用的。', '2024-10-04 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始考虑如何改进用户体验。我进行了一些用户调研，收集了用户的反馈和建议。根据这些反馈，我计划在下个版本中添加一些新功能，比如任务的重复设置和提醒功能。', '2024-10-07 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我实现了任务的重复设置功能。用户可以为任务设置每日、每周或每月的重复频率。这一功能的添加让应用变得更加灵活和实用。为了确保功能的正确性，我还编写了相应的测试用例。', '2024-10-09 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为应用添加了提醒功能。用户可以设置任务的提醒时间，应用会在任务到期前发送提醒通知。我使用了浏览器的Notification API来实现这一功能。经过测试，提醒功能工作正常。', '2024-10-11 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始优化应用的性能。通过分析发现，任务列表的渲染速度还有提升空间。我使用了React的lazy loading和代码分割技术，显著提高了应用的加载速度和响应速度。', '2024-10-14 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为应用添加了离线支持。使用Service Worker和IndexedDB技术，即使用户在没有网络连接的情况下，也能正常使用应用。离线数据会在网络恢复后自动同步到服务器。', '2024-10-16 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为项目添加了更多的测试用例，特别是集成测试和端到端测试。通过这些测试，可以确保应用的各个部分在一起工作时没有问题。测试覆盖率进一步提高，项目的稳定性也得到了保障。', '2024-10-18 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始考虑如何增加项目的可维护性。我重构了一些代码，使其更加模块化和易于理解。我还添加了一些代码注释和文档，帮助未来的开发者更容易上手项目。', '2024-10-21 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为项目添加了错误监控和日志记录功能。我使用了Sentry来捕获和记录应用中的错误，以便及时发现和修复问题。这一功能的添加提高了项目的可靠性。', '2024-10-23 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我发布了项目的第一个正式版本。经过几个月的努力，项目终于达到了一个比较完善的状态。我在GitHub上发布了Release版本，并感谢所有为项目做出贡献的开发者。', '2024-10-25 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始计划下一个开源项目。经过这段时间的开发和学习，我感觉自己在技术和项目管理方面都有了很大的提升。我希望能继续挑战自己，开发更多有趣和有用的项目。', '2024-10-28 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我研究了一些新的技术和工具，比如GraphQL和TypeScript。我认为这些技术可以在我的下一个项目中使用，帮助我开发出更高效和可维护的应用。', '2024-10-30 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为自己制定了学习计划，准备在接下来的时间里深入学习这些新技术。我相信通过不断学习和实践，自己会变得越来越优秀。', '2024-11-01 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始了新技术的学习。我选择了GraphQL作为第一个学习的对象，通过阅读文档和实践，我逐渐掌握了如何使用GraphQL来构建高效的API。我还尝试将GraphQL集成到现有的任务管理应用中，效果不错。', '2024-11-04 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我继续学习TypeScript。我发现TypeScript在大型项目中非常有用，可以帮助我写出更加健壮和可维护的代码。我将现有项目的一部分代码重写为TypeScript，虽然有些挑战，但收获很大。', '2024-11-06 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始构思下一个开源项目的想法。我希望能结合GraphQL和TypeScript，开发一个更加复杂和有挑战性的应用。我还在GitHub上创建了新的仓库，开始编写项目的初步文档。', '2024-11-08 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我开始搭建新项目的基本结构。我选择了使用Node.js和Express来开发后端，前端依然使用React。这次我决定从一开始就使用TypeScript，以提高代码的可维护性和可靠性。', '2024-11-11 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我实现了项目的基本功能，包括用户注册、登录和数据的增删改查。我还使用GraphQL来构建API接口，发现它在处理复杂查询和数据关系时非常高效。', '2024-11-13 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为项目添加了单元测试和集成测试。我使用Jest和Supertest来编写测试用例，确保每个功能都能正常工作。测试覆盖率逐渐提高，让我对项目的质量更有信心。', '2024-11-15 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我继续优化项目的性能和用户体验。我使用了React的Suspense和Lazy Loading来优化前端的加载速度，还为后端添加了一些缓存机制，提高了数据查询的效率。', '2024-11-18 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我为项目添加了更多的功能，比如用户的个人设置和通知系统。我还使用了WebSocket来实现实时通信，让应用变得更加互动和实时。', '2024-11-20 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);
insert into diary(content, entryTime, userIdentityGuid, isDeleted) values ('今天我发布了项目的第一个测试版本，邀请了一些朋友和同学来试用并提供反馈。我在GitHub上写了详细的发布说明，并感谢所有帮助和支持我的人。经过这三个月的努力，我感觉自己在技术和项目管理方面都有了很大的提升。我会继续努力，开发更多有趣和有用的开源项目。', '2024-11-22 12:00:00', '04be6c0f-205b-487e-89ad-8aebb11b32f1', false);

CREATE TABLE summary (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content TEXT NOT NULL,
    entrySince TIMESTAMP NOT NULL,
    entryTo TIMESTAMP NOT NULL,
    userIdentityGuid VARCHAR(255) NOT NULL,
    isDeleted TINYINT(1) NOT NULL DEFAULT 0,
    INDEX idx_entrySince (entrySince),
    INDEX idx_entryTo (entryTo),
    INDEX idx_userIdentityGuid (userIdentityGuid),
    INDEX idx_isDeleted (isDeleted)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;